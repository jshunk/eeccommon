#pragma once

#include <cstddef>
#include <cstdio>
#include <cstdint>

typedef int64_t			i64;
typedef int32_t			i32;
typedef int16_t			i16;
typedef int8_t 			i8;
typedef uint32_t		u32;
typedef uint64_t		u64;
typedef uint16_t		u16;
typedef uint8_t			u8;
typedef float			f32;
typedef double			f64;
typedef wchar_t			wch;
#if defined ESSENTIAL_PLATFORM_OSX || defined ESSENTIAL_PLATFORM_IOS
typedef intptr_t		ptr;
#else
typedef uintptr_t		ptr;
#endif
#ifndef ssize_t
typedef std::intmax_t	ssize_t;
#endif

static_assert(sizeof(i64) == 8);
static_assert(sizeof(i32) == 4);
static_assert(sizeof(i8) == 1);
static_assert(sizeof(u32) == 4);
static_assert(sizeof(u64) == 8);
static_assert(sizeof(u16) == 2);
static_assert(sizeof(u8) == 1);
static_assert(sizeof(f32) == 4);
static_assert(sizeof(f64) == 8);
